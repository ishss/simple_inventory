<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// https://api.github.com/search/repositories?q=javascript

Route::get('/category', function (Request $request) {

	$query = $request->get('q');

	$category = \App\Category::like('name', $query)->get();

 	return $category->toJson();

});
