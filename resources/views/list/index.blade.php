
@extends('layouts.master')

@section('title', 'Inventārs')

@section('content')


    <div class="row">
        <div class="col-md-6" style="border-right: 1px solid #c0c0c0">

            <div class="row">
                <div class="col-md-12">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                        <input type="search" class="form-control" id="search" aria-describedby="search">
                    </div>

                    <div id="filter" class="hidden">
                        filtrs
                    </div>

                </div>
            </div>
            <p></p>
            <table class="table table-bordered table-compact">
                <thead>
                <tr>
                    <!-- statusu realizēt kā krāsu rindai -->
                    <th>Kategorija</th>
                    <th>Nosaukums</th>
                    <th width="220">Piezīmes</th>
                </tr>
                </thead>
                <tbody>
                @foreach($items as $item)
                    <tr class="{{ $item->status->context or '' }}">
                        <td>
                            <a href="#">{{ $item->category->name }}</a>
                        </td>
                        <td>
                            <strong><a href="#">{{ $item->name }}</a></strong>
                        </td>
                        <td>
                            @if(isset($item->location->user))
                                {{ $item->location->user }} |
                                {{ $item->location->location['Korpuss'] or ''  }},
                                <strong>{{ $item->location->location['Kabinets'] or ''  }}</strong>

                            @else
                                -
                            @endif
                            <br/>
                            {{ rand(0,3) }} <i class="glyphicon glyphicon-comment"></i>,
                            <i class="glyphicon glyphicon-thumbs-up"></i>

                        </td>

                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $items->links() }}

        </div>

        <div class="col-md-6">

            <ul class="nav nav-tabs" id="tabi" role="tablist">
                <li role="presentation" class="disabled">
                    <a href="#invoice" role="tab" id="invoice-tab" data-toggle="tab" aria-controls="invoice" aria-expanded="false">
                        <i class="glyphicon glyphicon-option-horizontal"></i>
                    </a>
                </li>

                <li role="presentation" class="active">
                    <a href="#new" id="new-tab" role="tab" data-toggle="tab" aria-controls="new" aria-expanded="true">
                        Jauns inventārs
                    </a>
                </li>
                <li role="presentation" class="">
                    <a href="#invoice" role="tab" id="invoice-tab" data-toggle="tab" aria-controls="invoice" aria-expanded="false">
                        Pavadzīmes
                    </a>
                </li>


                <li role="presentation" class="dropdown">
                    <a href="#" class="dropdown-toggle" id="myTabDrop1" data-toggle="dropdown" aria-controls="myTabDrop1-contents">
                        Konfigurācija
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="myTabDrop1" id="myTabDrop1-contents">
                        <li>
                            <a href="#dropdown1" role="tab" id="dropdown1-tab" data-toggle="tab" aria-controls="dropdown1">
                                Statusi
                            </a>
                        </li>
                        <li>
                            <a href="#category" role="tab" id="category-tab" data-toggle="tab"  aria-controls="category">
                                Kategorijas
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>

            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade active in" role="tabpanel" id="new" aria-labelledby="home-tab">

                    <form>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nosaukums</label>
                            <input type="text" class="form-control" placeholder="Nosaukums">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Kategorija</label>
                            <select name="category" class="form-control">
                                @foreach($categories as $category)
                                <option value="{{ $category->id }}"> {{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="row" style="margin-bottom:15px">
                            <div class="col-md-6">
                                <label for="exampleInputPassword1">Status</label>
                                <select class="form-control">
                                    @foreach($status as $st)
                                        <option value="{{ $st->id }}">{{ $st->description or $st->name }}</option>
                                    @endforeach
                                    <option>Labota, remontēta</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for="exampleInputPassword1">Skaits</label>
                                <input type="number" class="form-control" id="exampleInputPassword1" placeholder="1">

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Pavadzīme</label>
                            <select class="form-control">
                                <option>Pavadzīmes nosaukums XX1</option>
                                <option>Pavadzīmes nosaukums XX2</option>
                                <option>Pavadzīmes nosaukums XX3</option>
                                <option>Pavadzīmes nosaukums XX4</option>
                            </select>
                            <span id="helpBlock" class="help-block">Sarakstā attēlotas 10 pēdējās pavadzīmes.</span>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputPassword1">Apraksts</label>
                            <textarea class="form-control"></textarea>
                        </div>


                        <button type="submit" class="btn btn-default">Pievienot</button>
                    </form>

                </div>

                <div class="tab-pane fade" role="tabpanel" id="invoice" aria-labelledby="invoice-tab">
                    <p>invoice</p>
                </div>

                <div class="tab-pane fade" role="tabpanel" id="dropdown1" aria-labelledby="dropdown1-tab">
                    <p>Etsy mixtape wayfarers      gluten-free, carles pitchfork biodiesel fixie etsy retro mlkshk vice blog. Scenester cred
                        you probably haven't heard of them, vinyl craft beer blog stumptown. Pitchfork sustainable tofu synth chambray yr.</p>
                </div>
                <!-- kategorjas pievienošanas skats -->
                <div class="tab-pane fade" role="tabpanel" id="category" aria-labelledby="category-tab">
                <p class="alert alert-info">
                    Jaunas kategorijas pievienošana
                </p>
                    <form>
                        <div class="form-group">
                            <label for="category">Kategorijas nosaukums</label>
                            <input name="category" type="text" class="form-control" placeholder="Kategorijas nosaukums">
                        </div>
                        <hr/>
                        <label for="parameters">Noklusējuma parametri</label>
                        <div class="row">
                            <div class="col-md-6">
                                <input type="text" class="form-control" placeholder="Parametra nosaukums">
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control" placeholder="Iespējamās vērtības">
                            </div>
                        </div>






                        <button type="submit" class="btn btn-default">Pievienot</button>
                    </form>


                </div>
            </div>



        </div>

    </div>





@endsection