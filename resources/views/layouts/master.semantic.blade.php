
<!DOCTYPE html>
<html>
<head>
    <!-- Standard Meta -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

    <!-- Site Properties -->
    <title>@yield('title')</title>
    <link rel="stylesheet" type="text/css" href="{{ url('css/semantic.min.css') }}">
    <style type="text/css">
        body > .ui.container {
            margin-top: 3em;
        }

        .ui.container > h2.dividing.header {
            font-size: 2em;
            font-weight: normal;
            margin: 3px 0 1em 0;
        }
        .ui.table {
            table-layout: fixed;
        }
    </style>
</head>
<body>

<div class="ui container">

    @yield('content')

</div>

<script src="{{ url('js/jquery-3.1.1.min.js') }}"></script>
<script src="{{ url('js/semantic.min.js') }}"></script>
<script>
    console.log('init...');
</script>

@stack('javascript')

</body>

</html>
