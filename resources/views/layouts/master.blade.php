
<!DOCTYPE html>
<html>
<head>
    <!-- Standard Meta -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

    <!-- Site Properties -->
    <title>@yield('title')</title>
    <link rel="stylesheet" type="text/css" href="{{ url('css/bootstrap.min.css') }}">
    <style type="text/css">
        .tab-content {
            color: #555;
            cursor: default;
            background-color: #fff;
            border: 1px solid #ddd;
            border-top-color: transparent;
            padding: 10px;
        }
    </style>
</head>
<body>

<div class="container">

    @include('layouts.header')

    @yield('content')

</div>

<script src="{{ url('js/jquery-3.1.1.min.js') }}"></script>
<script src="{{ url('js/bootstrap.min.js') }}"></script>
<script>
    console.log('init...');
</script>

@stack('javascript')

</body>

</html>
