
@extends('layouts.master')

@section('title', 'Inventārs')

@section('content')


    <div class="ui grid">
        <div class="six wide column">
            <div class="ui fluid input">
                <input type="text" placeholder="Nosaukums...">
            </div>
        </div>

        <div class="two wide column">
            <div class="ui right labeled input">
                <input type="text" placeholder="" style="width: 50px">
                <div class="ui basic label">
                    skaits
                </div>
            </div>
        </div>

        <!-- search category -->
        <div class="eight wide column">
            <div class="ui input search fluid">
                <input class="prompt" type="text" placeholder="Kategorija...">
                <div class="results"></div>
            </div>
        </div>

        <div class="sixteen wide column">
            <div class="ui fluid input">
                <input type="text" placeholder="kaut kas...">
            </div>
        </div>

    </div>



@endsection

@push('javascript')
    <script>


    </script>


@endpush

