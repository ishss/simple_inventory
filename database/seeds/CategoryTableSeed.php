<?php

use Illuminate\Database\Seeder;

class CategoryTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category')->insert(['name' =>'Pagarinātāji']);
        DB::table('category')->insert(['name' =>'UPS un sprieguma stabilizatori']);
        DB::table('category')->insert(['name' =>'Akumulatori ups']);
        DB::table('category')->insert(['name' =>'UPS aksesuāri']);
        DB::table('category')->insert(['name' =>'Stiprinājumi LED / LCD monitoriem']);
        DB::table('category')->insert(['name' =>'Baterijas portatīvajiem datoriem']);
        DB::table('category')->insert(['name' =>'Dzesētāji portatīvajiem datoriem']);
        DB::table('category')->insert(['name' =>'Portatīvie datori']);
        DB::table('category')->insert(['name' =>'Slēdzenes portatīvajiem datoriem']);
        DB::table('category')->insert(['name' =>'USB atmiņas kartes']);
        DB::table('category')->insert(['name' =>'Ārējie cietie diski']);
        DB::table('category')->insert(['name' =>'Atmiņas kartes']);
        DB::table('category')->insert(['name' =>'Operatīvā atmiņa']);
        DB::table('category')->insert(['name' =>'Cietie diski (SSD)']);
        DB::table('category')->insert(['name' =>'Cietie diski (HDD)']);
        DB::table('category')->insert(['name' =>'Cietie diski (Mobile)']);
        DB::table('category')->insert(['name' =>'Atmiņas karšu lasītāji']);
    }
}
