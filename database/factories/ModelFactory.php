<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Item::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->catchPhrase,
        'category_id' => rand(1,15),
        'status_id' => 1,
        'invoice_id' => rand(1,10),
        'user_id'   => 1,
        'updated_at' => null,
        'created_at' => \Carbon\Carbon::now()
    ];
});

$factory->define(App\Location::class, function (Faker\Generator $faker) {
    return [
        'status_id' => 1,
        'invoice_id' => rand(1,10),
        'user_id'   => 1,
        'updated_at' => null,
        'created_at' => \Carbon\Carbon::now()
    ];
});